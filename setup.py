"""BeSt evaluator"""

from setuptools import setup, find_packages
from codecs import open
from os import path


here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='best_evaluation',
    version='0.1',
    description='Scorer for DEFT belief and sentiment annotations',
    long_description=long_description,
    url='https://bitbucket.org/dbauer/best_evaluation',
    author='Daniel Bauer',
    author_email='bauer@cs.columbia.edu',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'best_evaluator=best_evaluator:main',
            'best_diagnostics=best_evaluator.diagnostics:main'
    ]}
)
